# Set the variables for drupal core and Pattern Lab.
# If none are supplied from the commandline, use defaults.
# No argument for building supplied, use arguments 1 & 2.
drupal_core_path=${1:-/var/www/drupal8}
pattern_lab_root_path=${2:-/var/www/html}

echo "Starting new Pattern Lab ..."
yarn install
echo "Done yarn install."

echo "Adding Javascripy symbolic links"
cd components/js
if [ -f "ready.min.js" ]; then
  rm ready.min.js
fi

if [ -f "drupal.js" ]; then
  rm drupal.js
fi

if [ -f "drupal.js" ]; then
  rm jquery.min.js
fi

ln -s "$drupal_core_path/core/assets/vendor/domready/ready.min.js"
ln -s "$drupal_core_path/core/misc/drupal.js"
ln -s "$drupal_core_path/core/assets/vendor/jquery/jquery.min.js"

echo "Done pattern-lab install."

gulp clean
gulp compile
gulp build

echo "Done rebuild.sh."
