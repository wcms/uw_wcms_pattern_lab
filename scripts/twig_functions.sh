#!/bin/bash
# Link contrib twig functions into the components directory

# ISTWCMS-2841: which drupal directory the profile is in.
drupal_directory=$(<./uw_wcms_pl_variables.sh)

# First we need to find out whether this is inside a Drupal install or standalone Emulsify
files=$(ls "/var/www/$drupal_directory/vendor/drupal-pattern-lab/bem-twig-extension/bem.function.php")
if [ $files ]
then
  # Drupal install
  for f in "/var/www/$drupal_directory/vendor/drupal-pattern-lab/bem-twig-extension/bem.function.php"; do

    ## Check if the glob gets expanded to existing files.
    ## If not, f here will be exactly the pattern above
    ## and the exists test will evaluate to false.
    if [ -e "$f" ]; then
        VENDORDIR=$(echo $f | sed "s/\\/drupal-pattern-lab\/bem-twig-extension\/bem.function.php//g")
    fi

    ## This is all we needed to know, so we can break after the first iteration
    break
  done
  
elif [ -f vendor/drupal-pattern-lab/bem-twig-extension/bem.function.php ]
then
  # Standalone
  VENDORDIR=vendor
elif [ -f ../../../vendor/drupal-pattern-lab/bem-twig-extension/bem.function.php ]
then
  # Composer-less Drupal
  VENDORDIR=../../../vendor
else
  # No vendor directory found
  echo "Vendor directory not found. Please run composer install."
fi

# If we found a vendor directory, copy the twig functions into place
if [ $VENDORDIR ]
then
  # Array of twig functions to copy, starting from the vendor directory
  twig_functions=(
    "drupal-pattern-lab/add-attributes-twig-extension/add_attributes.function.php"
    "drupal-pattern-lab/bem-twig-extension/bem.function.php"
  )

  # Create symlinks for all contrib twig functions
  for i in "${twig_functions[@]}"
  do
    cp $VENDORDIR/$i ./components/_twig-components/functions/.
  done
fi
