(function($) {
  Drupal.behaviors.local_site_footer = {
    attach: function (context, settings) {

      'use strict';

      $('.content-moderation-entity-moderation-form').parent().addClass('content-moderation-control');
      $('.content-moderation-entity-moderation-form').parent().insertAfter($('.uw-local-site-footer .tabs__nav'));
    }
  };
})(jQuery);
