(function($) {
  Drupal.behaviors.header_video = {
    attach: function (context, settings) {

      $('.uw-sp-header-video--controls button').click(function () {
        if ($(this).hasClass('pause')) {
          $(".uw-sp-header-video--video video").get(0).play();
          $(this).removeClass('pause');
        }
        else {
          $(".uw-sp-header-video--video video").get(0).pause();
          $(this).addClass('pause');
        }
      });

    }
  };
})(jQuery);