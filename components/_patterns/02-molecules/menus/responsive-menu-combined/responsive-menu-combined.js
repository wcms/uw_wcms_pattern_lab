(function($) {
  Drupal.behaviors.responsive_menu_combined = {
    attach: function (context, settings) {

      'use strict';

      // When an input in the responsive menu changes, check for placement.
      $(".menu__item input[type=checkbox]").change(function() {

        // Step through each li of the responsive menu and remove the class current_more_than_one.
        $('.responsive-nav__tab .with-sub-menu input ~ a.has-sub-menu').each(function() {
          $(this).parent().removeClass('current');
          $(this).parent().css('top', '');
        });

        // If we are checking the input, add the class current_more_than_one for position.
        if( this.checked ) {

          // Add class to li element.
          $(this).parent().addClass('current');

          // Ensure that we are more than one menu level deep and add class.
          if($('.responsive-nav__tab .with-sub-menu input:checked~a.has-sub-menu').length > 1) {

            // Calculate top height for li element.
            var top_height = 40 * ($('.responsive-nav__tab .with-sub-menu input:checked~a.has-sub-menu').length - 1);

            // Subtract the top height for the li element.
            $(this).parent().css('top', '-' + top_height + 'px');
          }
        }
        // Input was unchecked.
        else {

          // Add class to li element.
          $('.responsive-nav__tab .with-sub-menu input:checked~a.has-sub-menu').last().parent().addClass('current');

          // Ensure that we are more than one menu level deep and add class.
          if($('.responsive-nav__tab .with-sub-menu input:checked~a.has-sub-menu').length > 1) {

            // Calculate top height for li element.
            var top_height = 40 * ($('.responsive-nav__tab .with-sub-menu input:checked~a.has-sub-menu').length - 1);

            // Subtract the top height for the li element.
            $('.responsive-nav__tab .with-sub-menu input:checked~a.has-sub-menu').last().parent().css('top', '-' + top_height + 'px');
          }
        }
      });
    }
  };
})(jQuery);
