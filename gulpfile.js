/* globals require */

(function () {
  'use strict';

  // General
  var gulp = require('gulp-help')(require('gulp'));
  var localConfig = {};

  localConfig = require('./local.gulp-config');
  require('emulsify-gulp')(gulp, localConfig);

  gulp.task('clean',function() {
    // do something here.
  } );
})();
